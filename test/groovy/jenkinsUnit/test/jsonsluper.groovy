package jenkinsUnit.test

import groovy.json.JsonOutput

def slurper = new groovy.json.JsonSlurper()
def result = slurper.parseText('{' +
        '"person":{"name":"Guillaume","age":33,"pets":["dog","cat"]}, ' +
        '"status":true' +
        '} ')
String statusVar = "status"
if ( result[statusVar] ) {
    System.out.println("status exists ")
    System.out.println(result.status)

}
statusVar = "status1"
if ( result[statusVar] ) {
  System.out.println( statusVar + " exists ")
  System.out.println(result.status1)

} else  {
  System.out.println(statusVar + " does not exists ")

}

def stringArray = slurper.parseText("""["a", "b", "c" ]""")
for ( String s : stringArray )
println("Found - " + s)


def  roleMap =  [:]
int i ;
for ( String s: ["a", "b", "c"] as String[]) roleMap.role[i++].name=s ;
def rolesString= JsonOutput.toJson(roleMap);

println(rolesString)
