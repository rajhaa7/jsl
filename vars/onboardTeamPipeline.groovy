import CICDEnvUtils

// This Model is used for non java callout Proxies
def call ( String apigee_env, String build_number) {

    envUtils = new CICDEnvUtils();
    def config = envUtils.getEnviromentConfigs()
    node {

        stage('build-setup') {
            node {
//          checkout scm
                echo "config.com.cicd.apigee =  ${config.apigee}"

                if (env.BRANCH_TYPE) {
                    currentBuild.displayName = "${env.BRANCH_TYPE}-${apigee_env}-${build_number}"
                } else {
                    currentBuild.displayName = "${apigee_env}-${build_number}"

                }
                echo " Build Name reset ${currentBuild.displayName}"
                echo " Apigee Maven Options : ${apigeeMavenGlobalOptions}"
                echo "proxyRootDirectory : ${proxyRootDirectory}"


                withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                  credentialsId   : config.apigee.mgmt.credential,
                                  usernameVariable: 'edgeUser',
                                  passwordVariable: 'edgePassword']]) {
                  if(config.apigee.mgmt.OAuth){
                    apigeeMavenOptions = apigeeMavenGlobalOptions +
                      "-Dusername=${env.edgeUser} " +
                      "-Dpassword=${env.edgePassword} " +
                      "-Dauthtype=oauth";
                  }
                  else{
                    apigeeMavenOptions = apigeeMavenGlobalOptions +
                      "-Dusername=${env.edgeUser} " +
                      "-Dpassword=${env.edgePassword} " +
                      "-Dauthtype=basic";
                  }

                    echo " Apigee Maven Options : ${apigeeMavenOptions}"
                }

            }
        }

    }
}
