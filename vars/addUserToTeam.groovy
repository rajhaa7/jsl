import com.cicd.*

import hudson.model.ChoiceParameterDefinition
import hudson.model.User

def call () {

  node {
    def formProperties;
    def jenkinsUser ;
    def utils = new CICDEnvUtils();
    def userUtil = new JenkinsUserUtils();


    stage ("init") {

      if ( userUtil.isBuildTriggeredByUser() ){
        echo "Build Triggered by : " + userUtil.getUsernameForBuild()
      } else {
        error " This Flow MUST be Triggered by a Jenkins User"
      }

      if ( ! utils){ error "Unable to create CICDEnvUtils class"}

      println utils.getEnviromentConfigs()

    }

    stage("user-team-info") {



      jenkinsUser = User.current().getId() ;

      choiceOrg = new ChoiceParameterDefinition('org', (utils.getEnviromentConfigs()).jenkins.forms.apigeeOrgs as String[], 'Org Name ');
      choiceRoleType = new ChoiceParameterDefinition('teamType', utils.getEnviromentConfigs().jenkins.forms.apigeeTeams as String[], 'Team Type')

      formProperties = input message: "Add User To Team", ok: 'Add',
        parameters: [choiceOrg,
                     string(defaultValue: 'demo',
                       description: 'Team Name', name: 'teamName', trim: true),
                     choiceRoleType,
                     string(defaultValue: 'username@com.cicd.apigee.com',
                       description: 'Edge User Id to Add to Team', name: 'edgeUserId', trim: true),
                     string(defaultValue: 'com.cicd.apigee.com',
                       description: "${jenkinsUser} Email Domain", name: 'emailDomain', trim: true)]

      echo "  ${formProperties.toString()}"

    }

    stage("validate-priviliges") {
      // is team already present
      def apigeeAdminId = "${jenkinsUser}@${formProperties.emailDomain}"
      echo "Check if ${apigeeAdminId} is admin on Team : ${formProperties.teamName}"
      def markelTeamRolesObj = new MarkelEdgeRoles();

      if ( markelTeamRolesObj.isUserTeamAdmin(apigeeAdminId , formProperties.teamName, formProperties.org)){
        echo "User ${jenkinsUser} is authorized to perform this operation "
      } else {
        error "User ${jenkinsUser} is not authorized to perform this operation "
      }
    }

  }
}
