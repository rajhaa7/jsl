import CICDEnvUtils
import hudson.model.Cause
import pom

// This Model is used for non java callout Proxies
def call(String envName, String build_number) {

  echo " Stating Deploy On Production Env = ${envName}"

  envUtils = new CICDEnvUtils();
  def config = envUtils.getEnviromentConfigs()
  //TODO
  String apigeeMavenGlobalOptions = envUtils.getApigeeMavenOptions(envName);
  //TODO
  String uatPreValidationEnv = "int"
  String apigeeTestMavenGlobalOptions = envUtils.getApigeeMavenOptions(uatPreValidationEnv);
  String edgeTestEnvName = config.apigee.edge.envs[uatPreValidationEnv].env
  String edgeEnvName = config.apigee.edge.envs[envName].env
  emailUtils = new EmailUtils();

  String proxyRootDirectory = "edge";
  def apigeeEnvInfo;
  def javaDirectoryExists = false;

    node {

      try {
      def nodeHome = tool name: 'NodeJS', type: 'nodejs'
      println "NodeHome: ${nodeHome}"
      if (fileExists("${nodeHome}/npm")) {
        println "${nodeHome}/npm"
      } else if (fileExists("${nodeHome}/bin/npm")) {
        println "${nodeHome}/bin/npm"
        nodeHome = "${nodeHome}/bin"
        println "new home ${nodeHome}"
      }

      String giturl = null
      String deployVersion = null

      stage('init') {

        deleteDir();
        TeamService service = new TeamService();

        if (!params.projectName) {
          error "Project Name is Required "
        }

        if (!params.team) {
          error "Team is Required "
        }

        if (!params.api) {
          error "API Name is Required "
        }

        if (!params.version) {
          error "Version is Required "
        }

        if (!service.userHasRoleDevOps(params.team)) {
          error "User is not Authorized to perform this activity"
        }

        if (config.apigee.edge.envs.dev) {
          apigeeEnvInfo = config.apigee.edge.envs.dev
        }

        currentBuild.displayName = "PD-${envName}-${build_number}"

        if (fileExists("edge")) {
          proxyRootDirectory = "edge"
        }

        echo "proxyRootDirectory : ${proxyRootDirectory}"

        if (fileExists("java")) {
          javaDirectoryExists = true
        }

      }

      stage('Checkout') {
        SCMUtils scmUtils = new SCMUtils()
        giturl = scmUtils.getRepoUrl(params.projectName, params.team, params.api)
        deployVersion = params.version
        git url: giturl, branch: 'develop', credentialsId: config.scm.credential
        def shell = new shell()
        shell.pipe("git checkout tags/${deployVersion} -b ${deployVersion}")
      }


      stage('functional Test') {
        if (envName.toLowerCase().trim().equals("uat")) {
          def executeFunTests = false;
          FunctionalTestUtils functionalTestUtils = new FunctionalTestUtils()
          String functionalTestJobName = functionalTestUtils.getFunctionalTestJobName("${params.projectName}", "${params.team}", "${params.api}")
          println("Using FT Job : ${functionalTestJobName}")
          boolean functionalTestJobExists = functionalTestUtils.localTestExists(functionalTestJobName)
          println("this is the functional test job name"+ functionalTestJobName)
          println("Does the functional Test job exist: "+functionalTestJobExists)
          if (functionalTestJobExists
            && envUtils.getConfig().functionalTest.enabled.rcDeploy) {
            executeFunTests = true
          }

          if (executeFunTests) {

            if (javaDirectoryExists) {
              dir('java') {
                runCommand "mvn package"
              }
            }

            dir(proxyRootDirectory) {
              echo "Package proxy"
              withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                credentialsId   : config.apigee.mgmt.credential,
                                usernameVariable: 'edgeUser',
                                passwordVariable: 'edgePassword']]) {
                if(config.apigee.mgmt.OAuth){
                  apigeeTestMavenOptions = apigeeTestMavenGlobalOptions+
                    "-Dusername=${env.edgeUser} " +
                    "-Dpassword=${env.edgePassword} " +
                    "-Dauthtype=oauth";
                }
                else{
                  apigeeTestMavenOption = apigeeTestMavenGlobalOptions +
                    "-Dusername=${env.edgeUser} " +
                    "-Dpassword=${env.edgePassword} " +
                    "-Dauthtype=basic";
                }


                runCommand "mvn compile "

                runCommand "mvn package " +
                  " -Ddeployment.suffix=cicd " +
                  apigeeTestMavenOptions

                if (fileExists("resources/edge/env/${edgeTestEnvName}/caches.json")) {
                  echo "upload Caches to ${edgeTestEnvName} "
                  runCommand "mvn " + apigeeTestMavenOptions +
                    " apigee-config:caches"
                }

                if (fileExists("resources/edge/env/${edgeTestEnvName}/targetServers.json")) {
                  echo "upload Target Servers"
                  runCommand "mvn " + apigeeTestMavenOptions +
                    " apigee-config:targetservers"
                }

                if (fileExists("resources/edge/env/${edgeTestEnvName}/kvms.json")) {
                  echo "upload KVMs"
                  runCommand "mvn  " + apigeeTestMavenOptions +
                    " apigee-config:keyvaluemaps "
                }

                echo "deploy the proxy bundle"
                runCommand "mvn apigee-enterprise:deploy " +
                  "-Ddeployment.suffix=cicd " +
                  apigeeTestMavenOptions

                runCommand "${nodeHome}/npm install"


                echo "load api product for integration test"
                runCommand "mvn " + apigeeTestMavenOptions + " apigee-config:apiproducts"

                echo "load api developer for integration test"
                runCommand "mvn " + apigeeTestMavenOptions + " apigee-config:developers "

                echo "load api developer app for integration test"
                runCommand "mvn " + apigeeTestMavenOptions + " apigee-config:apps"

                echo "export app key for integration test"
                runCommand "mvn apigee-config:exportAppKeys " + apigeeTestMavenOptions;
                // "-Ddeployment.suffix=cicd " ;
              }
            }

            // Launch the Functional Test Job
            String status = functionalTestUtils.runLocalFunctionalTest(functionalTestJobName, null)
            if (status != 'SUCCESS') {
              currentBuild.result = 'FAILURE'
              error "Functional Tests Failed. Deploy Aborted"
              throw new Exception("Functional Test Failure")
            }


          } else {
            if ( ! functionalTestJobExists ) println ("Functional Test Job Did not exist")
            if ( ! envUtils.getConfig().functionalTest.enabled.rcDeploy ) {
              println( 'RC Pre Deploy Validation was not enabled ')
            }
            echo "Skipping Functional Testing"
          }
        }
      }



      stage('deploy-proxy') {


        if (javaDirectoryExists) {
          dir('java') {
            runCommand "mvn package"
          }
        }

        dir(proxyRootDirectory) {
          echo "Package proxy"
          withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                            credentialsId   : config.apigee.mgmt.credential,
                            usernameVariable: 'edgeUser',
                            passwordVariable: 'edgePassword']]) {
            if(config.apigee.mgmt.OAuth){
              apigeeMavenOptions = apigeeMavenGlobalOptions +
                "-Dusername=${env.edgeUser} " +
                "-Dpassword=${env.edgePassword} " +
                "-Dauthtype=oauth";
            }
            else{
              apigeeMavenOptions = apigeeMavenGlobalOptions +
                "-Dusername=${env.edgeUser} " +
                "-Dpassword=${env.edgePassword} " +
                "-Dauthtype=basic";
            }

            runCommand "mvn clean compile "

            runCommand "mvn package " +
              " -Ddeployment.suffix=cicd " +
              apigeeMavenOptions

            if (fileExists("resources/edge/env/${edgeEnvName}/caches.json")) {
              echo "upload Caches "
              runCommand "mvn " + apigeeMavenOptions +
                " apigee-config:caches"
            }

            if (fileExists("resources/edge/env/${edgeEnvName}/targetServers.json")) {
              echo "upload Target Servers"
              runCommand "mvn " + apigeeMavenOptions +
                " apigee-config:targetservers"
            }

            if (fileExists("resources/edge/env/${edgeEnvName}/kvms.json")) {
              echo "upload KVMs"
              runCommand "mvn  " + apigeeMavenOptions +
                " apigee-config:keyvaluemaps "
            }

            echo "deploy the proxy bundle"
            runCommand "mvn apigee-enterprise:deploy " +
              "-Ddeployment.suffix=cicd " +
              apigeeMavenOptions

          }
        }
      }

        if (config.jenkins.emailOnSuccess) {
          currentBuild.result = 'SUCCESS'
          emailUtils.emailNotificationSuccess()
        }
    }catch (any) {
        currentBuild.result = 'FAILURE'
        emailUtils.emailNotificationFailure()
      }

  }
}


def getApigeeMavenOptions(Map apigeeEnvInfo) {
  def apigeeMavenOptions = " -Papigee  " +
    "-Denv=${apigeeEnvInfo.env} " +
    "-Dorg=${apigeeEnvInfo.org} " +
    "-Dusername=${env.edgeUser} " +
    "-Dpassword=${env.edgePassword} ";
  return apigeeMavenOptions as String;
}

def getUsernameForBuild() {
  def causes = currentBuild.rawBuild.getCauses()
  for (Cause cause in causes) {
    def user;
    if (cause instanceof hudson.model.Cause.UserIdCause) {
      hudson.model.Cause.UserIdCause userIdCause = cause;
      user = userIdCause.getUserName()
      return user
    }
  }
  return null
}

def isBuildCauseUserAction() {
  def causes = currentBuild.rawBuild.getCauses()
  for (Cause cause in causes) {
    if (cause instanceof hudson.model.Cause.UserIdCause) return true
  }
  return false
}

def getPom() { return new pom(); }

def runCommand(String command) {
  if (!isUnix()) {
    println command
    bat returnStdout: true, script: "${command}"
  } else {
    println command
    sh returnStdout: true, script: command
  }
}
