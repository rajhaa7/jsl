
def call ( String branch_name) {

  node {
    stage("test-user-in-role") {

      echo "This is a test "

      try  {

        ApigeeUtils  apigeeUtils = new ApigeeUtils();

      if (apigeeUtils.isUserInRole("ajayy@sidglobal.net", "sidgs_admin", "sidgs")) {
        echo "PASS"
      } else {
        echo "FAIL"
      }
    }catch (Exception e) {
        e.printStackTrace()
        error e.getMessage()

      }
    }
    stage("test-add-user-to-role") {

      echo "This is a test "

      try  {

        ApigeeUtils  apigeeUtils = new ApigeeUtils();

        if (apigeeUtils.addUserToRole("ajayy@sidglobal.net", "sidgs_admin", "sidgs")) {
          echo "PASS"
        } else {
          echo "FAIL"
        }
      }catch (Exception e) {
        e.printStackTrace()
        error e.getMessage()

      }
    }
    stage("logger-test") {
      LogUtils log = new LogUtils();
      echo ("log props: ${log.logConfig}")
      log.info("This is info")
      log.debug("this is debug ")
      log.error("this is error")
      log.warn("this is warn")
    }
  }

}
