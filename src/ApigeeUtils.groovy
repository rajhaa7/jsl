@Grab(group = 'org.codehaus.groovy.modules.http-builder',
  module = 'http-builder', version = '0.7')

import groovy.transform.Field

@Field LogUtils log = new LogUtils();
@Field CICDEnvUtils envUtils = new CICDEnvUtils();
@Field HttpUtils httpUtil = new HttpUtils();

String getHttpResponse(def response) {
  def httpStatus = response.status
  if (response.status != 404 && (httpStatus >= 100 && httpStatus <= 399)) {
    return response.content as String
  } else {
    ""
  }
}

boolean roleExists(String roleName, String org) {

  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;

  String url = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${org}/userroles/${roleName}"

  def responseData = httpRequest contentType: 'APPLICATION_JSON',
    acceptType: "APPLICATION_JSON", httpMode: 'GET',
    customHeaders: [
      [name: 'Authorization', value: getAuthToken()]
    ],
    url: "${url}", validResponseCodes: "200,404"


  int httpStatus = responseData.status as Integer
  if (httpStatus == 404) return false
  else return true

}

void removeRole(String roleName, String org) {

  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;
  String url = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${org}/userroles/${roleName}"

  log.debug("Sending DELETE - ${url}")
  String[] usersInRole = getUsersInRole(roleName, org)
  for (String user : usersInRole) {
    removeUserFromRole(user.trim(), roleName, org)
  }

  httpUtil.doDelete(url, null, getAuthToken())
  log.debug("Deleted : role ${roleName} for org ${org}")

}

void addRole(String roleName, String org) {

  if (roleExists(roleName, org)) {
    log.info("Role ${roleName} Exists in Org ${org}")
    return
  }

  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;
  String url = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${org}/userroles"

  def payload = """ { "role": [
                  {"name" : "${roleName}"}
            ]} """

  log.info("Sending - ${payload as String}")
  httpRequest contentType: 'APPLICATION_JSON',
    acceptType: "APPLICATION_JSON", httpMode: 'POST',
    customHeaders: [
      [name: 'Authorization', value: getAuthToken()]
    ],
    url: "${url}",
    requestBody: payload as String

//  httpUtil.doPost(url, payload as String , getAuthToken())
  log.debug("Added : role ${roleName} for org ${org}")

}

void addApiToTeam(String roleName, String orgName, String payload) {

  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;
  String url = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/o/${orgName}/userroles/${roleName}/permissions"
  log.info("Sending - ${payload as String} to ${url}")
  if (!envUtils.getConfig().apigee.echo_http) {
    httpRequest contentType: 'APPLICATION_JSON',
      acceptType: "APPLICATION_JSON", httpMode: 'POST',
      customHeaders: [
        [name: 'Authorization', value: getAuthToken()]
      ],
      url: "${url}",
      requestBody: payload as String
  }
  log.debug("Added : Permission to role ${roleName} ")
}


void addUserToRole(String username, String roleName, String org) {

  if (isUserInRole(username, roleName, org)) {
    log.info("User ${username} is already in Role ${roleName}")
    return
  }
  log.debug("addUserToRole -->> Start ")

  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;
  String url = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${org}/userroles/${roleName}/users"
  log.debug("Sending POST request ${url}?id=${username}");

  def response = httpRequest contentType: 'APPLICATION_FORM',
    httpMode: 'POST',
    customHeaders: [[name: 'Authorization', value: getAuthToken()]],
    url: "${url}?id=${username}", validResponseCodes: "200,404"

  if (response.status == 404) {
    log.error("User ${username} NOT added to role ${roleName} for org ${org}. " +
      "User or Org May Not exist")
    return
  }

  log.debug("User ${username} added to role ${roleName} for org ${org}")
  log.debug("addUserToRole -->> finish ")

}

void removeUserFromRole(String username, String roleName, String org) {
  if (!isUserInRole(username, roleName, org)) {
    log.info("User ${username} is not in Role ${roleName}")
    return
  }

  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;
  String url = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${org}/userroles/${roleName}/users/${username}"

  log.debug("Sending DELETE request ${url}");

  httpUtil.doDelete("${url}", null, getAuthToken())
  log.debug("User ${username} removed from role ${roleName} for org ${org}")

}

boolean isUserInRole(String username, String roleName, String org) {

  log.debug("IsUserInRole -->> Start ")

  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;

  String url = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${org}/userroles/${roleName}/users"

  log.debug("Sending request to ${url}");

  def response = httpUtil.doGet(url, null, getAuthToken())

  if ( response.status == 200 ) {
    String  resp  = getHttpResponse(response)
    if ( resp &&  resp.toLowerCase().contains(username.toLowerCase())) {
      log.debug("User ${username} is in role ${roleName} for org ${org}")
      return true
    }
  }

  log.debug("IsUserInRole -->> finish ")

  return false

}

/**
 * Set the permissions on a role
 * @param roleName
 * @param orgName
 * @param permissions
 */
void setResourcePermissions(String roleName, String orgName, String permissions) {
  String url = getApigeeServerUrl() + "/o/${orgName}/userroles/${roleName}/resourcepermissions"
  log.debug("Doing POST on ${url} using Data : ${permissions}")
  httpRequest contentType: 'APPLICATION_JSON',
    httpMode: 'POST',
    customHeaders: [[name: 'Authorization', value: getAuthToken()]],
    url: url, requestBody: permissions
}

String[] getUsersInRole(String roleName, String org) {
  log.debug("getUsersInRole -->> Start ")
  def mgmtServerInfo = envUtils.getConfig().apigee.mgmt;
  String usersInRole = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${org}/userroles/${roleName}/users"

  log.debug("Sending request to ${usersInRole}");
  def response = httpUtil.doGet(usersInRole, null, getAuthToken())

  String[] users = getHttpResponse(response).replace("[", "").replace("]", "").replace("\"", "").split(",")
  log.debug(users.toString())
  return users
}


protected Map<String, String> getMgmtServerCreds() {
  return envUtils.getCredentials(envUtils.getConfig().apigee.mgmt.credential)
}

protected String getAuthToken() {
  def creds = envUtils.getCredentials(envUtils.getConfig().apigee.mgmt.credential)
  if (creds.username && creds.password) {
    authToken = getBasicAuthCreds(creds.username, creds.password)
    return authToken
  } else {
    log.fatal("Credential " + envUtils.getConfig().apigee.mgmt.credential + " is not setup correctly ")
  }
}

def getBasicAuthCreds(String u, String p) {
  return httpUtil.getBasicAuthCreds(u, p)
}

def getApigeeServerUrl() {
  return new EdgeRoles().getApigeeServerUrl(envUtils.getConfig().apigee.mgmt)
}

return this;
