import groovy.transform.Field

@Field private CICDEnvUtils envUtils = new CICDEnvUtils();
@Field private LogUtils log = new LogUtils();
@Field private ApigeeUtils apigeeUtil = new ApigeeUtils();


/**
 * Add a team and setup permissions associated to the team
 * @param apiName
 * @param orgName
 */
def addApiToTeams(String apiName) {
  // apigee.org.markel-prod.roleDetail.Dev.permissions.addApi

  orgs = envUtils.getConfig().apigee.org.list

  def teamName = apiName.split("-")[0];
  String apiEdgeName = apiName.split("-")[1];
  envUtils.getConfig().apigee.org.list.each { orgName ->
    log.info("addApi -->>> Got Org : ${orgName} for Team :${teamName} ")
    envUtils.getConfig().apigee.org[orgName].roles.each { roleName ->
      log.info("addApi -->>> Got Role : ${roleName} on Org : ${orgName} for Team: ${teamName} ")
      def orgInfo = envUtils.getConfig().apigee.org[orgName]
      if ( orgInfo.roleDetail[roleName] && orgInfo.roleDetail[roleName].permissions ) {
        def resourceName = orgInfo.roleDetail[roleName].permissions.addApi
        if (resourceName) {
          log.info("Getting Permissions from : ${resourceName}")
          def payload = libraryResource resourceName.trim()
          def configmap = [:]
          configmap["apiName"] = apiName
          payload = envUtils.getTemplate().transform(payload as String, configmap)
          log.info("adding to ${teamName}_${roleName} : ${payload}")
          apigeeUtil.addApiToTeam("${teamName}_${roleName}", orgName, payload)

        } else {
          log.info("Skipping Adding Api permission is not enabled ")
        }
      } else {
        log.warn ("No Permissions for ${teamName}_${roleName} ")
      }
    }
  }
}


/**
 * Add a team and setup permissions associated to the team
 * @param teamName
 * @param orgName
 */
def addTeam(String teamName, String orgName, String teamAdmin) {

  log.info("addTeam -->>> ${teamName} on ${orgName} ")
  def orgInfo = envUtils.getConfig().apigee.org[orgName];
  if (!orgInfo) {
    log.fatal("Unable to Find Inforamtion for Org : ${orgName}")
  }

  log.info("Org Info : ${orgInfo}")

  // Roles for the
  orgInfo.roles.each { s ->
    log.debug(s)
    // add the role is not already present
    def rolename = "${teamName}_${s}"
    ApigeeUtils apigeeUtil = new ApigeeUtils();
    if (! apigeeUtil.roleExists(rolename, orgName)) {
      log.info("Adding Role: ${rolename}  to Organization :  ${orgName}")
      apigeeUtil.addRole("${rolename}", orgName)

      if (orgInfo.roleDetail[s]) {

        def jsonTemplate = orgInfo.roleDetail[s].template
        def tempParams = orgInfo.roleDetail[s].parameters
        log.info("Using template ${jsonTemplate} with params ${tempParams}")

        log.info("Adding Role Permissions on ${rolename}  to Organization :  ${orgName}")
        String roleDef = new template().getResourceAsString(jsonTemplate, tempParams)
        log.info("Setting up Permissions for the Team ${teamName} on Organization ${orgName}")
        apigeeUtil.setResourcePermissions(rolename, orgName, roleDef)

        log.info("Completed Role Setup : ${rolename}")
      } else {
        log.warn("Skipping: Add Role Permissions on ${rolename}  to Organization :  ${orgName} : Configuration Not Present")
      }
    } else {
      log.warn("Skipping: Add Role ${rolename}  to Organization :  ${orgName} : Role Already Exists ")
    }
  }

//  log.info("Adding Administrators ${teamName}_admin for the Team : ${teamName}")
  if ( ! apigeeUtil.roleExists("${teamName}_admin", orgName)) {
    apigeeUtil.addRole("${teamName}_admin", orgName)
  } else {
    log.warn("Skipping: Add Role ${teamName}_admin to Organization :  ${orgName} : Role Already Exists ")
  }


}

/**
 * Add a admin user to a team ; This method checks if the user requesting this action is a team admin
 * If the user is not a team admin, this request will be rejected
 * @param userid
 * @param adminDomain
 * @param teamName
 * @param teamType
 * @param orgName
 */
def addTeamAdmin(String userid, String teamName, String orgName) {

  log.info("Adding Team Admin : ${userid} to team: ${teamName} on Org: ${orgName}")
  Map orgInfo = envUtils.getConfig().apigee;
  apigeeUtil.addUserToRole(userid, teamName, orgName)
}

/**
 * Add a user to a team ; This method checks if the user requesting this action is a team admin
 * If the user is not a team admin, this request will be rejected
 * @param userid
 * @param adminDomain
 * @param teamName
 * @param teamType
 * @param orgName
 */
boolean addUserToTeam(String userid, String adminDomain, String teamName, String teamType, String orgName) {

  Map orgInfo = envUtils.getConfig().apigee;

  JenkinsUserUtils userUtils = new JenkinsUserUtils()
  def currentUserId = userUtils.getUserIdForBuild()

  if (currentUserId == null) {
    log.fatal("Unable to User Triggering this Build. Failing Build")
  }

  log.info("Current User Id: ${currentUserId}")

  if (!adminDomain) {
    adminDomain = orgInfo.email_domain
  }

  boolean currentUserIsTeamAdmin = apigeeUtil.isUserInRole("${currentUserId}@${adminDomain}", "${teamName}_admin", orgName)
  if (currentUserIsTeamAdmin) apigeeUtil.addUserToRole(userid, teamName + "_" + teamType, orgName)
  else {
    log.fatal("Current User : ${currentUserId} is not an Admin on Team ${teamName}. This Operation is not Permitted")
    return false
  }

  return true
}

/**
 * Checks if a the current logged in jenkins user is a part of an Apigee Team
 * @param teamName
 * @return
 */
boolean jenkinsUserInTeam( String teamName ){

  JenkinsUserUtils userUtils = new JenkinsUserUtils()
  def currentUserId = userUtils.getUserIdForBuild()
  def userDomain = envUtils.getConfig().apigee.email_domain
  envUtils.getConfig().apigee.org.list.each { orgName ->
    if( apigeeUtil.isUserInRole(currentUserId, teamName, orgName)) return  true
  }

  return false
  }

/**
 * Checks if a the current logged in jenkins user is a part of a DevOps Team
 * @param team
 * @return
 */
boolean userHasRoleDevOps( String team ){

  def teamName ;
  if ( ! team.trim().endsWith("DevOps") ) {
    teamName = team.trim()+ "_DevOps"
  } else teamName = team
  JenkinsUserUtils userUtils = new JenkinsUserUtils()
  def currentUserId = userUtils.getUserIdForBuild()
  def userDomain = envUtils.getConfig().apigee.email_domain

  log.info("Checking Current User Id : ${currentUserId}@${userDomain} in Role ${teamName}")

  boolean userIsDevOps = false
  envUtils.getConfig().apigee.org.list.each { orgName ->
    if( apigeeUtil.isUserInRole("${currentUserId}@${userDomain}", teamName, orgName)) {
      userIsDevOps = true
    }
  }

  return userIsDevOps
}

/**
 * Checks if a the current logged in jenkins user is a part of a DevOps Team
 * @param team
 * @return
 */
boolean userHasRoleAdmin( String team ){

  def teamName ;
  if ( ! team.trim().endsWith("admin") ) {
    teamName = team.trim()+ "_admin"
  } else teamName = team
  JenkinsUserUtils userUtils = new JenkinsUserUtils()
  def currentUserId = userUtils.getUserIdForBuild()
  def userDomain = envUtils.getConfig().apigee.email_domain
  envUtils.getConfig().apigee.org.list.each { orgName ->
    if( apigeeUtil.isUserInRole(currentUserId, teamName, orgName)) return  true
  }

  return false
}



return this;
