import groovy.transform.Field

@Field private static Map ftFinderMap = null ;

/**
 * Used to get the Name of a Functional Test Associated to an API
 * @param teamName
 * @param apiName
 * @return String representing the Functional Test Job Name
 */
String getFunctionalTestJobName (String projectName, String teamName, String apiName ) {
  def envUtils = new CICDEnvUtils();
  String functionalTestJobName = "";

  // some times a repo may now follow convention
  // in this case lookup the name of the FT Job from the FT Test List
  functionalTestJobName = findAlternateFtJobName(projectName, teamName, apiName )
  if ( functionalTestJobName != null ) return functionalTestJobName

  // A explicit functional Test Job was Not Defined.. Use Convention to derive the FT Job Name
  if ( envUtils.getConfig().functionalTest.prefix) {
    functionalTestJobName+=envUtils.getConfig().functionalTest.prefix
  }
  functionalTestJobName += "${teamName}_${apiName}-funcTest"
  return functionalTestJobName
}

private String findAlternateFtJobName(String projectName, String teamName, String apiName) {

  println "Getting FT Job Name for ${projectName} - ${teamName} - ${apiName}"

  def envUtils = new CICDEnvUtils();
  if ( ftFinderMap == null &&
        envUtils.getConfig().functionalTest &&
        envUtils.getConfig().functionalTest.finder) {
    // Check if a job has been registered for the projectName\teamName_apiName combination
    String funcTestConfig = envUtils.getConfig().functionalTest.finder;
    def raw = libraryResource funcTestConfig
    def slurper = new groovy.json.JsonSlurperClassic()
    ftFinderMap = slurper.parseText(raw as String);
  }

  if ( ftFinderMap == null ) {
    println "ftMinderMap is Null"
    return null
  } else {
    if ( ftFinderMap[projectName][teamName][apiName] )
    return  ftFinderMap[projectName][teamName][apiName].ftJobName as String
    else null
  }

}

/**
 * @param jobName - Functional Test Job Name
 * @return
 */
boolean localTestExists(String jobName ) {
  boolean status ;
  if ( jobName.toLowerCase().startsWith("http")) {
     return false ;
  }
  Jenkins.instance.getAllItems().each {it ->
    String currentJobName = it.fullName ;
    if ( currentJobName.trim().toLowerCase().equals(jobName.toLowerCase().trim())) {
//      println currentJobName.trim().toLowerCase() + " == " + jobName.toLowerCase().trim()
      status = true
    } else
    {
//      println currentJobName.trim().toLowerCase() + " != " + jobName.toLowerCase().trim()

    }
  }
  return status  ;
}

/**
 * @param jobName - Functional Test Job Name
 * @return
 */
boolean exactFunctionalTestJobName(String jobName ) {
  String name = null ;
  Jenkins.instance.getAllItems(AbstractProject.class).each {it ->
    String currentJobName = it.fullName ;
    if ( currentJobName.trim().toLowerCase().equals(jobName.toLowerCase().trim())) {
      name = it.fullName
    }
  }
  return name  ;
}

/**
 *
 * @param jobName
 * @param params
 * @return
 */
String runLocalFunctionalTestNoWait ( String jobName, Map params ) {
  build job: jobName, parameters: params , wait: false
}

/**
 *
 * @param jobName
 * @param params
 * @return
 */
String runLocalFunctionalTest ( String jobName, Map params) {
  def result = build job: jobName, parameters: params , wait: true
  return  result.result
}

String runLocalJob ( String jobName, Map params=null) {
  if ( params != null ) {
    def result = build job: jobName, parameters: params, wait: true
    return result.result
  }else {
    def result = build job: jobName, wait: true
    return result.result
  }
}


return this
