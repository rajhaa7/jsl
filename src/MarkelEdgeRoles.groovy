


  private getEdgeroles() {
    return new EdgeRoles()
  }

  private getenvutils() {
    return (new CICDEnvUtils())
  }


  private getConfiguration() {
    return (new CICDEnvUtils()).getEnviromentConfigs()
  }

/**
 * Add a new role to Markel
 * @param rolename
 * @return
 */
  def addrole(String rolename, String requestingUser) {

    String nonProdName = getConfiguration().apigee.org.nonProd.name as String;
    String prodName = getConfiguration().apigee.org.prod.name as String;

    getEdgeroles().createRoleNonProd(rolename, nonProdName)
    getEdgeroles().createRoleProd(rolename, prodName)

  }

  def isUserTeamAdmin(String username, String roleName, String org) {

    // rolename is always teamname_role
    String teamName = (roleName.split("_"))[0]
    // get user teams from Apigee API
    String teamAdminRole = teamName + "_admin"


    def mgmtServerInfo = getConfiguration().apigee.mgmt;

    println("MGMT Server Info : - " + mgmtServerInfo)

    def creds = getenvutils().getCredentials(mgmtServerInfo.credential)

    String usersInRole = new EdgeRoles().getApigeeServerUrl(mgmtServerInfo) +
      "/organization/${org}/userroles/${teamAdminRole}/users"

    echo "Sending request to ${usersInRole}";

    boolean auth = false;
    def AUTH;
    if (creds.username && creds.password) {
      println("AUTH : -- " + AUTH)
      AUTH = "Basic " + "${creds.username}:${creds.password}".bytes.encodeBase64().toString()
      auth = true
    } else {
      error "Credential " + getConfiguration().apigee.mgmt + " is not setup correctly "
    }

    if (auth) {
      println("AUTH : -- " + AUTH)

      def response = httpRequest acceptType: 'APPLICATION_JSON',
        httpMode: 'GET',
        customHeaders: [[name: 'Authorization', value: AUTH]],
        url: usersInRole
//    requestBody :roleData

      String responseData = response.content as String;
      if (responseData.toLowerCase().contains(username.toLowerCase().trim())) {
        return true
      }
    } else {
      // This should never happen
      error "Something really BAD happened !!! "
    }
    return false

  }


return this ;
