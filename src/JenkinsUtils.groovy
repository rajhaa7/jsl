
boolean localJobExists(String jobName ) {
  Jenkins.instance.getAllItems(AbstractProject.class).each {it ->
    String currentJobName = it.fullName ;
    if ( currentJobName.trim().toLowerCase().equals(jobName.toLowerCase().trim())) {
      return true
    }
  }
  return false ;
}


return this;
