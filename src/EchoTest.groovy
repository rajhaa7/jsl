

def getTemplate() { new template() }

def sayHello() {
//  EchoTest "hello - Shared Libs have Loaded"
}

def echoTemplate() {
  def sample= libraryResource "config/sample-template.json"
  def sampleTemplate = getTemplate().transform(sample, [tv1:"this is a test"])
  echo "${sampleTemplate}"

}
return this

