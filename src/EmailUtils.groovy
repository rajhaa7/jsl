import hudson.model.Cause
import CICDEnvUtils


def emailNotificationSuccess() {
  envUtils = new CICDEnvUtils();
  def config = envUtils.getEnviromentConfigs();
  if(isBuildCauseUserAction()){
    String userEmail = getUserIdForBuild()+'@'+config.apigee.email_domain
   //def commiterEmail = runCommand("git --no-pager show -s --format=%ae")
    emailext(
      subject: "SUCCESS: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: '${JELLY_SCRIPT, template="apimgmt_default-email-template"}',
      attachLog: true,
      to: "${userEmail}"
    )
  }

}

def emailNotificationFailure() {
  envUtils = new CICDEnvUtils();
  def config = envUtils.getEnviromentConfigs();
  if(isBuildCauseUserAction()){
    String userEmail = getUserIdForBuild()+'@'+config.apigee.email_domain

    //def commiterEmail = runCommand("git --no-pager show -s --format=%ae")
    emailext(
      subject: "FAILURE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: '${JELLY_SCRIPT, template="apimgmt_default-email-template"}',
      attachLog: true,
      to: "${userEmail}"
    )
  }
}


def boolean isBuildCauseUserAction() {
  def causes = currentBuild.rawBuild.getCauses()
  for (Cause cause in causes) {
    println("The Cause is "+ cause)
    if (cause instanceof hudson.model.Cause.UserIdCause) return true
  }
  return false
}

String getUserIdForBuild() {
  def causes = currentBuild.rawBuild.getCauses()
  for (Cause cause in causes) {
    def user;
    if (cause instanceof hudson.model.Cause.UserIdCause) {
      hudson.model.Cause.UserIdCause userIdCause = cause;
      user = userIdCause.getUserId()
      return user
    }
  }
  return null
}

def runCommand(String command) {
  if (!isUnix()) {
    println command
    bat returnStdout: true, script: "${command}"
  } else {
    println command
    sh returnStdout: true, script: command
  }
}
