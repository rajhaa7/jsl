

import com.cicd.apigee.CustomRole
import groovy.json.JsonOutput

def getenvutils() {
  return new CICDEnvUtils();
}

private getApigeeServerUrl (Map mgmtInfo) {
  String mgmtUrl = "${mgmtInfo.apiProtocol}://${mgmtInfo.apiHost}:${mgmtInfo.apiPort}/${mgmtInfo.version}"
  return  mgmtUrl
}


def createTeam (String teamName, String orgname ) {

  def mgmtServerInfo = getenvutils().getEnviromentConfigs().apigee.mgmt;
  def creds = getenvutils().getCredentials(mgmtServerInfo.credentials)

  String[] roleList = getenvutils().getEnviromentConfigs().apigee.org[orgname].teams as String[]

  String createRoleUrl = getApigeeServerUrl(mgmtServerInfo)+
    "/o/${orgname}/userroles"

  def AUTH = (new HttpUtils()).getBasicAuthCreds(creds.username as String, creds.password as String);

  def  roleMap =  [:]
  roleMap.put("role", new ArrayList());
  for ( String s: roleList ) {
    roleMap.role << new CustomRole(name: "${teamName}_${s}");
  }
  roleMap.role << new CustomRole(name: "${teamName}");
  def rolesString=  JsonOutput.toJson(roleMap);

  println("Sending :" + rolesString )

  def response = httpRequest acceptType: 'APPLICATION_JSON',
    httpMode: 'POST',
    customHeaders: [[name: 'Authorization', value: AUTH]],
    url: createRoleUrl,
    requestBody: rolesString.toString()

  echo "${response.getContent}"
}

def createTeamInProd (String teamName, String orgname) {
  def mgmtServerInfo = getenvutils().getEnviromentConfigs().apigee.mgmt;
  def roles =[teamName, teamName+"_devOps", teamName+"_PM", teamName+"_admin"]
  def creds = getenvutils().getCredentials(mgmtServerInfo.credentials)

  String createRoleUrl = getApigeeServerUrl(mgmtServerInfo)+
    "/o/${orgname}/userroles"

  echo "Sending request to ${createRoleUrl}"

  def AUTH = "Basic " + "${creds.username}:${creds.password}".bytes.encodeBase64().toString()
  System.out.println (AUTH)

  String roleData =  """{"role" : [
          {"name": "${roles[0]}"},
          {"name": "${roles[1]}"},
          {"name": "${roles[2]}"},
          {"name": "${roles[3]}"}
    ]}"""

  def response = httpRequest acceptType: 'APPLICATION_JSON',
    httpMode: 'POST',
    customHeaders: [[name: 'Authorization', value: AUTH]],
    url: createRoleUrl,
    requestBody :roleData

  echo "${response.getContent}"

}

def addUserToRole(String orgName, String username, String roleName) {

  def mgmtServerInfo = getenvutils().getEnviromentConfigs().apigee.mgmt;
  def roles =[rolename, rolename+"_devOps", rolename+"_PM"]
  def creds = getenvutils().getCredentials(mgmtServerInfo.credentials)
  def AUTH = "Basic " + "${creds.username}:${creds.password}".bytes.encodeBase64().toString()

  String addUsertoRole = getApigeeServerUrl(mgmtServerInfo) +
    "/organizations/${orgName}/userroles/${rolename}/users?id=${username}"

  def response = httpRequest acceptType: 'APPLICATION_FORM',
    httpMode: 'POST',
    customHeaders: [[name: 'Authorization', value: AUTH]],
    url: addUsertoRole
}
