#!groovy

@Library('cicd') _

/**
 * Note This Pipeline requires the following Parameters
 * teamName : Sub Team (ex demo_devOps) to add user to
 * emailDomain : email domain for signed in user
 * userId : Apigee User being added to team
 */

//validateCicdSetup ("dev")
CICDEnvUtils utils = new CICDEnvUtils()

node {
    stage("add-user-to-team") {
        println("Adding User ${params.userId} to Team ${params.teamName} ")
        TeamService service = new TeamService();
        utils.getConfig().apigee.org.list.each { orgName ->
            String[] teamInfo = (params.teamName as String).split("_");
            boolean response = service.addUserToTeam(params.userId, params.emailDomain, teamInfo[0], teamInfo[1], orgName)
          if ( response != true ){
            error "Error Adding User. Refer Console Logs for Details"
            currentBuild.result = 'FAILURE'
          }
        }
    }

}

