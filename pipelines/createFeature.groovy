#!groovy

@Library('cicd') _


node {

  CICDEnvUtils utils = new CICDEnvUtils()
  def userUtil = new JenkinsUserUtils();
  SCMUtils scmUtils = new SCMUtils()


  stage("init") {
    if (userUtil.isBuildTriggeredByUser()) {
      echo "Build Triggered by : " + userUtil.getUsernameForBuild()
    } else {
      error " This Flow MUST be Triggered by a Jenkins User"
    }
    if (!new TeamService().userHasRoleDevOps(params.team)) {
      error " User is NOT a member of ${params.team}_DevOps. NOT Authorized to run this Job"
    }
  }

  if ((params.operation as String).equals("feature-create")) {
    stage("feature-create") {
      new BranchManagerService().createFeature(params.feature, params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("release-candidate-create")) {
    stage("release-candidate") {
      new BranchManagerService().createReleaseCandidate(params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("feature-close")) {
    stage("feature-close") {
      new BranchManagerService().finishFeature(params.feature, params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("release-start")) {
    stage("release-start") {
      new BranchManagerService().createRelease(params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("release-close")) {
    stage("release-close") {
      new BranchManagerService().finishRelease(params.team, params.project, params.api)
    }
  }


  if ((params.operation as String).equals("hotfix-start")) {
    stage("hotfix-start") {
      new BranchManagerService().createHotFix(params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("hotfix-close")) {
    stage("hotfix-close") {
      new BranchManagerService().finishHotFix(params.team, params.project, params.api)
    }
  }
}

