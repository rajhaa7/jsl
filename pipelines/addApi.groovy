#!groovy

@Library ('cicd') _

node {
  stage ("add-api-to-team") {

    TeamService service = new TeamService();
    service.addApiToTeams(params.apiName)
  }
}

