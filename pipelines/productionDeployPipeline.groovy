#!groovy

/**
 * Jenkinsfile to be used for Production Deployment for Markel API Pipelines with Java Call Out Archetype
 */

@Library('cicd') _

properties([[$class  : 'BuildDiscarderProperty',
             strategy: [$class: 'LogRotator', numToKeepStr: '10']]])


if ( !params.environment ) error ("Environment Name is Required")

productionDeliveryPipeline params.environment , env.BUILD_NUMBER

