#!groovy

@Library('cicd') _


node {

    CICDEnvUtils utils = new CICDEnvUtils()
    def userUtil = new JenkinsUserUtils();
    SCMUtils scmUtils = new SCMUtils()

    stage ("init") {
        if ( userUtil.isBuildTriggeredByUser() ){
            echo "Build Triggered by : " + userUtil.getUsernameForBuild()
        } else {
            error " This Flow MUST be Triggered by a Jenkins User"
        }

        if ( ! new TeamService().jenkinsUserInTeam("params.team")+"_DevOps"){
            error " User is NOT a member of ${params.team}_DevOps. NOT Authorized to run this Job"
        }

        }

    stage("checkout") {
        def url = scmUtils.getRepoUrl(params.project, params.team, params.api)
        scmUtils.checkOutRepo(url, "develop")
    }

    stage("create-feature") {

        if( scmUtils.existsBranch("${params.feature}")) {
            error  "Feature : ${params.feature} exist. Stopping Job "
        }

        String creds = scmUtils.getRepoCredentialId();
        withCredentials([usernamePassword(credentialsId: creds , passwordVariable: 'password', usernameVariable: 'username')]) {
            mavenRunner = new Maven()
            String goal = "jgitflow:feature-start"
            String commands = "-DscmUsername=${env.username} -DscmPassword=${env.password}  -DfeatureName=${params.name} --batch-mode"
            // some block
            mavenRunner.runMaven commands, goal
        }


    }

    stage("feature-start") {

        if( scmUtils.existsBranch("${params.feature}")) {
            error  "Feature : ${params.feature} exist. Stopping Job "
        }

        String creds = scmUtils.getRepoCredentialId();
        withCredentials([usernamePassword(credentialsId: creds , passwordVariable: 'password', usernameVariable: 'username')]) {
            mavenRunner = new Maven()
            String goal = "jgitflow:feature-start"
            String commands = "-DscmUsername=${env.username} -DscmPassword=${env.password}  -DfeatureName=${params.name} --batch-mode"
            // some block
            mavenRunner.runMaven commands, goal
        }


    }


    stage("release-start") {

        if( scmUtils.existsBranch("rel-")) {
            error  "Feature : A Release Branch Already exist. Only One Release Branch is permitted. Stopping Job"
        }

        String creds = scmUtils.getRepoCredentialId();
        withCredentials([usernamePassword(credentialsId: creds , passwordVariable: 'password', usernameVariable: 'username')]) {
            mavenRunner = new Maven()
            String goal = "jgitflow:release-start"
            String commands = "-DscmUsername=${env.username} -DscmPassword=${env.password}  -DallowSnapshots=false --batch-mode"
            // some block
            mavenRunner.runMaven commands, goal
        }

    }


    stage("hotfix-start") {

        if( scmUtils.existsBranch("rel-")) {
            error  "Feature : A Release Branch Already exist. Only One Release Branch is permitted. Stopping Job"
        }

        String creds = scmUtils.getRepoCredentialId();
        withCredentials([usernamePassword(credentialsId: creds , passwordVariable: 'password', usernameVariable: 'username')]) {
            mavenRunner = new Maven()
            String goal = "jgitflow:release-start"
            String commands = "-DscmUsername=${env.username} -DscmPassword=${env.password}  -DallowSnapshots=false --batch-mode"
            // some block
            mavenRunner.runMaven commands, goal
        }

    }
}

