#!groovy

@Library('cicd') _

validateCicdSetup("dev")

node {

    CICDEnvUtils utils = new CICDEnvUtils()
    def userUtil = new JenkinsUserUtils();


    stage ("init") {

        if ( userUtil.isBuildTriggeredByUser() ){
            echo "Build Triggered by : " + userUtil.getUsernameForBuild()
        } else {
            error " This Flow MUST be Triggered by a Jenkins User"
        }
    }

    stage("add-team") {
        TeamService service = new TeamService();
        utils.getConfig().apigee.org.list.each { orgName ->
            // Add team to the organization
            service.addTeam(params.teamName, orgName, params.teamAdminEmail)
        }
    }

    stage("add-team-admin") {
        TeamService service = new TeamService();
        utils.getConfig().apigee.org.list.each { orgName ->
            //Add Admin  to teams
            service.addTeamAdmin(params.teamAdminEmail, params.teamName + "_admin", orgName)
        }
    }

}

