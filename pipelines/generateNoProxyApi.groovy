#!groovy

@Library('cicd') _

validateCicdSetup("dev")

node {

  CICDEnvUtils utils = new CICDEnvUtils()
  def userUtil = new JenkinsUserUtils();


  stage ("init") {

    if ( userUtil.isBuildTriggeredByUser() ){
      echo "Build Triggered by : " + userUtil.getUsernameForBuild()
    } else {
      error " This Flow MUST be Triggered by a Jenkins User"
    }

    println("Clear the workspace")
    deleteDir()

  }

  stage("validate-input") {

    if ( ! params.team ) {
      error "Team Name is Required"
    }

    if ( ! params.apiName )
      error "API Name is required"
  }

  stage("generate-api") {
    CICDEnvUtils envUtils = new CICDEnvUtils()
    String settingsFile = envUtils.getConfig().maven.settingsFileId
    mavenRunner = new Maven();
    println "using Maven Settings File with Name : ${settingsFile}"
    withMaven(globalMavenSettingsConfig: settingsFile) {
      def command ="-DarchetypeGroupId=com.markel.archetype " +
        " -DgroupId=ignore.this " +
        " -Dpackage=ignorethis " +
        " -DarchetypeArtifactId=api-pass-through " +
        " -DarchetypeVersion=1.0.0-SNAPSHOT " +
        " -DartifactId=${params.apiName} " +
        " -DteamName=${params.team} " +
        " -DinteractiveMode=false "

      if ( isUnix() )
      sh "mvn ${command} archetype:generate"
      else bat "mvn ${command} archetype:generate"
    }

    def downloadUrl = "${env.BUILD_URL}/execution/node/20/ws"
    echo "API Generated Can be Found Here : ${downloadUrl}"
  }

}

